package invoiceproject.database;
import invoiceproject.bean.Product;

public class ProductData {
	public static Product[] products = createProducts();
	
	public static Product[] createProducts()
	{
		Product[] products = new Product[5];
		
		products[0] = new Product(1,"P1",200);
		products[1] = new Product(2,"P2",100);
		products[2] = new Product(3,"P3",300);
		products[3] = new Product(4,"P4",600);
		products[4] = new Product(5,"P5",900);
		return products;
	}

	public static Product[] getProducts() {
		return products;
	}
	
}
