package invoiceproject.database;
import invoiceproject.bean.Invoice;

public class InvoiceData {

	private static Invoice []invoices = new Invoice[5];

	public boolean addInvoice(Invoice invoice)
	{
		for (int i = 0; i < invoices.length; i++) {
			if(invoices[i] == null)
			{
				invoices[i] = invoice;
				return true;
			}
		}
		return false;
	}
	public Invoice getInvoiceByNumber(String invno)
	{
		Invoice invoice = null;
		for (int i = 0; i < invoices.length; i++) {
			if(invoices[i].getInvno().equals(invno))
			{
				invoice = invoices[i];
				break;
			}
		}
		return invoice;
	}


}
