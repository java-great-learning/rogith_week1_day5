package invoiceproject.database;
import invoiceproject.bean.Customer;


public class CustomerData {
public static Customer[] customers = createCustomer();
	
	public static Customer[] createCustomer()
	{
		Customer[] customers = new Customer[5];
		
		customers[0] = new Customer("Rogith","Chennai","Rogith@gmail.com","9080003582");
		customers[1] = new Customer("Arun","Chennai","Arun@gmail.com","9080003581");
		customers[2] = new Customer("Ishq","Chennai","Ishq@gmail.com","9080003583");
		customers[3] = new Customer("Lee","Chennai","Lee@gmail.com","9080003584");
		customers[4] = new Customer("Pope","Chennai","Pope@gmail.com","9080003586");
		return customers;
	}

	public static Customer[] getCustomers() {
		return customers;
	}
	
	
	
	
}
