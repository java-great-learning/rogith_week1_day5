package invoiceproject.bean;
import java.time.LocalDate;
import java.util.Arrays;

public class Invoice {
	private String invno;
	private Customer customer;
	private LocalDate date;
	private LineItem[] lineItem;
	private int totalBill;
	
	public Invoice(String invno, Customer customer, LocalDate date, LineItem[] lineItem) {
		super();
		this.invno = invno;
		this.customer = customer;
		this.date = date;
		this.lineItem = lineItem;
	}

	public String getInvno() {
		return invno;
	}

	public void setInvno(String invo) {
		this.invno = invno;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LineItem[] getLineItem() {
		return lineItem;
	}

	public void setLineItem(LineItem[] lineItem) {
		this.lineItem = lineItem;
	}

	public int getTotalBill() {
		return totalBill;
	}

	public void setTotalBill(int totalBill) {
		this.totalBill = totalBill;
	}

	@Override
	public String toString() {
		return "Invoice [invno=" + invno + ", customer=" + customer + ", date=" + date + ", lineItem="
				+ Arrays.toString(lineItem) + ", totalBill=" + totalBill + "]";
	}
	
	
}
