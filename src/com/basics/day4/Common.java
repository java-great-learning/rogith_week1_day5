package com.basics.day4;

public interface Common {
	
	void markAttendance();
	void dailyTask();
	void displayDetails();
}
